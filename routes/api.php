<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//List all reservations 
Route::get('/reservations', 'ReservationController@index');
//List single reservation
Route::get('/reservation/{reservation}', 'ReservationController@show');
//Create new reservation
Route::post('/reservation', 'ReservationController@store');
Route::put('/reservation', 'ReservationController@store');