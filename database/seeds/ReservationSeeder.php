<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ReservationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=1; $i < 50; $i++) { 
            \DB::table('reservations')->insert(array(
                'id' => $i,
                'idSeat' => $i,
                'idMovie' => $i,
                'status' => $faker->word,
                'authToken' => $faker->word
            ));
        }
    }
}
