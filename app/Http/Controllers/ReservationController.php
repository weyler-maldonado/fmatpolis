<?php

namespace App\Http\Controllers;

use App\Reservation;
use Illuminate\Http\Request;
use App\Http\Resources\Reservation as ReservationResource;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = Reservation::all();
        return ReservationResource::collection($reservations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reservation = $request->isMethod('put') ? Reservation::findOrFail($request->id) : new Reservation;
        $status = $request->input('status');
        if($status == 'reservar')
        {
            $reservation->id = $request->input('id');
            $reservation->idSeat = $request->input('idSeat');
            $reservation->idMovie = $request->input('idMovie');
            $reservation->status = $request->input('status');
            $reservation->authToken = $request->input('authToken');
            if($reservation->save())
            {
                return response()->json([
                    'status' => 'reservado'
                ]);
            }
            else 
            {
                return response()->json([
                    'Exception' => 'Algo falló'
                ]);
            }

        }
        elseif ($status == 'comprado') 
        {
            $reservation = Reservation::find($request->id);
            $reservation->id = $request->input('id');
            $reservation->idSeat = $request->input('idSeat');
            $reservation->idMovie = $request->input('idMovie');
            $reservation->status = $request->input('status');
            $reservation->authToken = $request->input('authToken');
            if($reservation->save())
            {
                return response()->json([
                    'status' => 'comprado'
                ]);
            }
            else 
            {
                return response()->json([
                    'Exception' => 'Algo falló'
                ]);
            }
            //echo "actualizado en BD";   
        }
        else 
        {
            return response()->json([
                'Exception' => 'Algo falló'
            ]);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        return new ReservationResource(Reservation::find($reservation->id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
    }
}
