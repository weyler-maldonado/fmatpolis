<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Reservation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'idSeat' => $this->idSeat,
            'idMovie' => $this->idMovie,
            'status' => $this->status,
            'authToken' => $this->authToken,
        ];
    }
}
